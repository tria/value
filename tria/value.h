#ifndef __TRIA_VALUE__
#define __TRIA_VALUE__

#include <iostream>
#include <string>

namespace tria {

	class value {
	protected:
		std::string _value;

		int                _get(int a)                const { return std::stoi  (_value); }
		long int           _get(long int a)           const { return std::stol  (_value); }
		long long          _get(long long a)          const { return std::stoll (_value); }
		unsigned long      _get(unsigned long a)      const { return std::stoul (_value); }
		unsigned long long _get(unsigned long long a) const { return std::stoull(_value); }

		float              _get(float a)              const { return std::stof (_value);  }
		double             _get(double a)             const { return std::stod (_value);  }
		long double        _get(long double a)        const { return std::stold(_value);  }

		std::string        _get(std::string a)        const { return _value;              }
		const char*        _get(const char* a)        const { return _value.c_str();      }

		value& _set(int val)                { _value = std::to_string(val); return *this; }
		value& _set(long int val)           { _value = std::to_string(val); return *this; }
		value& _set(unsigned long val)      { _value = std::to_string(val); return *this; }
		value& _set(long long val)          { _value = std::to_string(val); return *this; }
		value& _set(unsigned long long val) { _value = std::to_string(val); return *this; }
		value& _set(float val)              { _value = std::to_string(val); return *this; }
		value& _set(double val)             { _value = std::to_string(val); return *this; }
		value& _set(long double val)        { _value = std::to_string(val); return *this; }
		value& _set(std::string val)        { _value = val;                 return *this; }
		value& _set(const char* val)        { _value = std::string(val);    return *this; }


	public:
		value() : _value("") {}
		template <class T>
		value(T val){
			set(val);
		}

		operator int()                const { return get<int>();                }
		operator long int()           const { return get<long int>();           }
		operator long long()          const { return get<long long>();          }
		operator unsigned long()      const { return get<unsigned long>();      }
		operator unsigned long long() const { return get<unsigned long long>(); }

		operator float()              const { return get<float>();              }
		operator double()             const { return get<double>();             }
		operator long double()        const { return get<long double>();        }

		operator std::string()        const { return get<std::string>();        }
		operator const char*()        const { return get<const char*>();        }

		template <class T>
		T get() const {
			return _get(T());
		}

		template <class T>
		value& set(T t) {
			return _set(t);
		}

		template <class T>
		value& operator=(T t) {
			return _set(t);
		}

		friend std::ostream& operator<<(std::ostream& os, const value& obj);
		friend std::istream& operator>>(std::istream& is, value& obj);

		friend bool operator==(const value & lhs, const value & rhs);
		friend bool operator!=(const value & lhs, const value & rhs);

		template <class T>
		bool operator==(const T & rhs) {
			return get<std::string>() == value(rhs).get<std::string>();
		}
		template <class T>
		bool operator!=(const T & rhs) {
			return get<std::string>() != value(rhs).get<std::string>();
		}

	};

	inline bool operator==(const value & lhs, const value & rhs) {
		return lhs.get<std::string>() == rhs.get<std::string>();
	}
	inline bool operator!=(const value & lhs, const value & rhs) {
		return lhs.get<std::string>() != rhs.get<std::string>();
	}

	inline std::ostream& operator<<(std::ostream& os, const value& obj){
		os << obj._value;
		return os;
	}
	inline std::istream& operator>>(std::istream& is, value& obj){
		std::string s;
		is >> s;
		obj = s;
		return is;
	}

}

#endif