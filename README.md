# tria::value

`tria::value` - small class that can contain string or number value.

# Features

- Seamless interaction - no need to implicitly set a type
- Intuitive interface - just work with it as if it's an ordinary variable
- Header-only - no compilation needed
- One to another - convert string <-> number with just an assignment

# TODO

[ ] convert checks
[ ] optimized version with math support

# Example

```cpp
tria::value a = 42;
tria::value b = "test";

cout << a << b;
cin >> a;

std::string a_txt = a; // or a.get<std::string>()
a = a_txt;

```